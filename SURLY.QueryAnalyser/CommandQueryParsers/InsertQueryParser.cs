﻿using System;
using System.Collections.Generic;
using System.Linq;
using SURLY.Models;

namespace SURLY.QueryAnalyser.CommandQueryParsers
{
    public class InsertQueryParser
    {
        public List<SURLYAttribute> GetAttributes(string query)
        {
            var cleanAttributesString = GetAttributesString(query);
            var getAttributes = GetAttributesList(cleanAttributesString);
            
            return FromListOfStringsToModel(getAttributes);
        }

        private string GetAttributesString(string query)
        {
            var splitQuery = query.Split(' ');
            //adding two because there are going to be spaces between the command and the relation name
            var index = splitQuery[0].Length + splitQuery[1].Length + 2;
            return query.Remove(0, index);
        }

        private List<string> GetAttributesList(string attributeString)
        {
            var temp = new CustomQueryParser().GetAsListOfStringsConsideringSpecialCharacters(attributeString);
            return temp;
        }

        private List<SURLYAttribute> FromListOfStringsToModel(List<string> valuesList)
        {
            var toReturn = new List<SURLYAttribute>();
            foreach (var value in valuesList)
            {
                var attribute = new SURLYAttribute();
                attribute.SetValue(value);
                toReturn.Add(attribute);   
            }

            return toReturn;
        } 
    }
}
