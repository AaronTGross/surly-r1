﻿using System.Collections.Generic;
using System.Linq;
using SURLY.Models;

namespace SURLY.QueryAnalyser.CommandQueryParsers
{
    public class ProjectQueryParser
    {
        public List<SURLYAttributeDefinition> GetAttributesDefinitions(string query)
        {
            var toReturn = new List<SURLYAttributeDefinition>();
            var cleanedAttributes = GetAttributeNames(query);

            foreach (var cleanedAttribute in cleanedAttributes)
            {
                toReturn.Add(new SURLYAttributeDefinition
                {
                    Name = cleanedAttribute
                });
            }

            return toReturn;
        }

        private List<string> GetAttributeNames(string query)
        {
            var skipLength = query.Split(' ')[0].Length + query.Split(' ')[1].Length + 2;
            var attributeString = query.Remove(0, skipLength);
            return attributeString.Split(' ').ToList();
        } 
    }
}
