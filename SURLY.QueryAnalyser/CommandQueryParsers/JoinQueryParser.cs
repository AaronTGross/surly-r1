﻿using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using SURLY.Models.Enums;
using SURLY.Models.Util;

namespace SURLY.QueryAnalyser.CommandQueryParsers
{
    public class JoinQueryParser
    {

        public List<string> GetRelations(string query)
        {
            //join is 4 letters and then remove the space
            query = query.Remove(0, 5);
            return GetRelationNames(query);
        }

        public List<SURLYCondition> GetJoinConditions(string query)
        {
            query = RemoveUpToOnKeyWord(query);
            var conditions = new CustomQueryParser().GetJoinConditionsStatements(query);
            return CreateConditions(conditions.ToList());
        }

        private List<SURLYCondition> CreateConditions(List<string> conditionStrings)
        {
            var toReturn = new List<SURLYCondition>();
            conditionStrings.ForEach(x =>
            {
                var subs = new CustomQueryParser().GetAsListOfStringsConsideringSpecialCharacters(x);
                if (subs[0].Contains('.') && subs[2].Contains('.'))
                {
                    var condition = new SURLYCondition
                    {
                        ConditionType = ConditionType.JOIN,
                        AttributeToCompare = subs[0],
                        ComparisonType = ComparisonTypeFactory.Get(subs[1]),
                        AttributeToCompareAgainst = subs[2]
                    };
                    toReturn.Add(condition);
                }
                else
                {
                    var condition2 = new SURLYCondition
                    {
                        ConditionType = ConditionType.OR,
                        AttributeToCompare = subs[0],
                        ComparisonType = ComparisonTypeFactory.Get(subs[1]),
                        ValueComparedAgainst = subs[2]
                    };

                    toReturn.Add(condition2);
                }

            });

            return toReturn;
        } 

        private string RemoveUpToOnKeyWord(string query)
        {
            var queryArray = query.Split(' ');
            var removeToIndex = 0;
            for (int i = 0; i < queryArray.Length; i++)
            {
                if (queryArray[i].ToLower() == "on")
                {
                    break;
                }
                else
                {
                    //added the plus one to handle the spaces after words
                    removeToIndex += queryArray[i].Length + 1;
                }
            }

            //added 3 extra in order to handle the "ON "
            return query.Remove(0, removeToIndex + 3);
        }

        private List<string> GetRelationNames(string query)
        {
            var relationNames = new List<string>();
            foreach (var sub in query.Split(','))
            {
                if (!sub.Trim().Contains(' '))
                {
                    relationNames.Add(sub);
                }
                else
                {
                    var subsSub = sub.Trim().Split(' ');
                    relationNames.Add(subsSub[0].Trim());
                }
            }

            return relationNames;
        }
    }
}
