﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SURLY.Models.Util;

namespace SURLY.QueryAnalyser
{
    public class CustomQueryParser
    {
        private int _quoteCount = 0;
        private int _numberOfParenthesis = 0;

        //before passing to this, clean the string as much as possible.. this should
        //just take in a singularList
        public List<string> GetAsListOfStringsConsideringSpecialCharacters(string attributes)
        {
            var substrings = BreakStringIntoListOfSubstrings(attributes, new BreakSymbols().GetSimpleBreaks());
            return substrings;
        }

        //this needs to a string of the conditiona   
        public List<string> GetConditionsConderingMultipleConditions(string conditions)
        {
            var conditionGroups = BreakStringIntoListOfSubstrings(conditions, new BreakSymbols().GetBreaksForGrouping());
            //split this on AND and OR
            var subConditions = new List<string>();
            foreach (var conditionGroup in conditionGroups)
            {
                var subs = BreakBasedUponWords(conditionGroup, "and", new string[] { "and" });
                subs.AddRange(BreakBasedUponWords(conditionGroup, "or", new string[] { "or" }));
                var distinct = subs.Distinct();
                foreach (var sub in distinct)
                {
                    var slicedAndDiced = BreakBasedUponWords(sub, "and", new string[] { "and" });
                    slicedAndDiced.AddRange(BreakBasedUponWords(sub, "or", new string[] { "or" }));
                    subConditions.AddRange(slicedAndDiced.Distinct());
                }
            }
            var getFormattedSubs = FormatSubConditions(subConditions.Where(x => !x.ToLower().Contains("and") && !x.ToLower().Contains("or")).Distinct(), conditions);

            return getFormattedSubs;
        }

        public List<string> GetJoinConditionsStatements(string conditions)
        {
            var conditionGroups = BreakStringIntoListOfSubstrings(conditions, new BreakSymbols().GetBreaksForGrouping());
            //split this on AND and OR
            var subConditions = new List<string>();
            foreach (var conditionGroup in conditionGroups)
            {
                var subs = BreakBasedUponWords(conditionGroup, "and", new string[] { "and" });
                subs.AddRange(BreakBasedUponWords(conditionGroup, "or", new string[] { "or" }));
                var distinct = subs.Distinct();
                foreach (var sub in distinct)
                {
                    var slicedAndDiced = BreakBasedUponWords(sub, "and", new string[] { "and" });
                    slicedAndDiced.AddRange(BreakBasedUponWords(sub, "or", new string[] { "or" }));
                    subConditions.AddRange(slicedAndDiced.Distinct());
                }
            }

            return subConditions.Where(x => !ContainsBreakWordThatIsNotPartOfAWord(x.ToLower(), "and") && !ContainsBreakWordThatIsNotPartOfAWord(x.ToLower(), "or")).Distinct().ToList();
        }

        private List<string> FormatSubConditions(IEnumerable<string> distinct, string orginalConditions)
        {
            var toReturn = new List<string>();
            var orginalConditionArray = orginalConditions.ToLower().Trim().Split(' ');
            foreach (var subCondition in distinct)
            {
                var subConditionArray = subCondition.ToLower().Split(' ');
                var index = 0;
                foreach (var word in orginalConditionArray)
                {
                    if (orginalConditionArray[index].Contains(subConditionArray[0]))
                    {
                        if (orginalConditionArray[index + 1].Contains(subConditionArray[1]))
                        {
                            if (orginalConditionArray[index + 2].Contains(subConditionArray[2]))
                            {
                                if (index == 0)
                                {
                                    toReturn.Add("or " + subCondition);
                                    break;
                                }
                                else
                                {
                                    toReturn.Add(orginalConditionArray[index - 1] + " " + subCondition);
                                    break;
                                }
                            }
                            else
                            {
                                index++;
                            }
                        }
                        else
                        {
                            index++;
                        }

                    }
                    else
                    {
                        index++;

                    }
                }
            }
            return toReturn;
        }

        private List<string> BreakBasedUponWords(string stringyThingy, string breakWord, string[] breakWordArray)
        {
            var segments = new List<string>();

            if (ContainsBreakWordThatIsNotPartOfAWord(stringyThingy.ToLower(), breakWord))
            {
                var brokenstring = stringyThingy.ToLower().Split(breakWordArray, StringSplitOptions.RemoveEmptyEntries);
                foreach (var broke in brokenstring)
                {
                    segments.AddRange(BreakBasedUponWords(broke.Trim(), breakWord, breakWordArray));
                }
            }

            segments.Add(stringyThingy);
            return segments;
        }

        private bool ContainsBreakWordThatIsNotPartOfAWord(string stringyThing, string breakWord)
        {
            if (stringyThing.Contains(breakWord))
            {
                for (var i = 0; i < stringyThing.Length; i++)
                {
                    if (stringyThing[i] == breakWord[0])
                    {
                        if (stringyThing[i + 1] == breakWord[1])
                        {
                            if (stringyThing[i + 2] == ' ')
                            {
                                return true;
                            }
                            if (breakWord.Length == 3 && stringyThing[i + 2] == breakWord[2])
                            {
                                if (stringyThing[i + 3] == ' ')
                                {
                                    return true;
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            return false;
        }

        private List<string> BreakStringIntoListOfSubstrings(string stringyThingy, List<string> breakCharacters)
        {
            var strings = new List<string>();
            var value = "";
            foreach (var character in stringyThingy.Trim())
            {
                if (!BreakCharacter(character, breakCharacters))
                {
                    value += character;
                }
                else
                {
                    if (!string.IsNullOrEmpty(value)) strings.Add(value);
                    value = "";
                }
            }
            if (!string.IsNullOrEmpty(value)) strings.Add(value);

            return strings;
        }


        private bool BreakCharacter(char character, List<string> breakCharacters)
        {
            var isBreakSymbol = true;
            IncDecCounters(character);
            foreach (var symbol in breakCharacters)
            {
                if (StopCheckingBreakCharacters(character))
                {
                    isBreakSymbol = false;
                    break;
                }
                isBreakSymbol = symbol.Contains(character);
                if (isBreakSymbol) break;
            }

            return isBreakSymbol;
        }

        private bool StopCheckingBreakCharacters(char character)
        {
            var openingQuote = (_quoteCount == 1);
            var openInsideOfParenthesis = (character == (char)40 && _numberOfParenthesis > 1);
            var closeInsideOfParenthesis = (character == (char)41 && _numberOfParenthesis > 0);
            return openingQuote || openInsideOfParenthesis || closeInsideOfParenthesis;
        }

        private void IncDecCounters(char character)
        {
            //this is checking for the '
            if (character == (char)39)
            {
                _quoteCount++;
                if (_quoteCount == 2)
                {
                    _quoteCount = 0;
                }
            }
            //this is for the (
            if (character == (char)40)
            {
                _numberOfParenthesis++;
            }
            //this is for the )
            if (character == (char)41)
            {
                _numberOfParenthesis--;
            }
        }
    }
}
