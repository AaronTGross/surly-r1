﻿using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using SURLY.Models.Enums;
using SURLY.Models.Util;
using System.Xml.Serialization;

namespace SURLY.Models
{
    public class SURLYRelation
    {
        public string RelationName;
        public List<SURLYAttributeDefinition> _definitions = new List<SURLYAttributeDefinition>();
        public LinkedList<SURLYTuple> _tuples { get; set; }

        //needed for serialization
        public SURLYRelation()
        {
            _tuples = new LinkedList<SURLYTuple>();
        }

        public SURLYRelation(string relationName)
        {
            RelationName = relationName;
            _tuples = new LinkedList<SURLYTuple>();
        }

        public SURLYRelationSerializable ToSerializable()
        {
            return new SURLYRelationSerializable
            {
                _tuples = _tuples.ToList(),
                RelationName = RelationName,
                _definitions = _definitions
            };
        }

        public void AddAttributeDefinitions(SURLYAttributeDefinition definition)
        {
            _definitions.Add(definition);
        }

        public void AddTuple(SURLYTuple tuple)
        {
            _tuples.AddLast(tuple);
        }

        public void AddTuples(List<SURLYTuple> tuples)
        {
            tuples.ForEach(x => _tuples.AddLast(x));
        }
        public void AddTuples(LinkedList<SURLYTuple> tuples)
        {
            foreach (var surlyTuple in tuples)
            {
                _tuples.AddLast(surlyTuple);
            }
        }

        public List<SURLYAttributeDefinition> GetTypesDefinitions()
        {
            return this._definitions;
        }

        public List<SURLYTuple> Select(List<SURLYCondition> selectiveConditions)
        {
            var toReturn = new List<SURLYTuple>();

            foreach (var selectiveCondition in selectiveConditions)
            {
                if (selectiveCondition.ConditionType == ConditionType.JOIN)
                {
                    continue;
                }

                var matchingTuples = GetMatchingTuples(selectiveCondition, _tuples);

                switch (selectiveCondition.ConditionType)
                {
                    case ConditionType.OR:
                        toReturn.AddRange(AddTuplesDistinct(toReturn, matchingTuples));
                        break;
                    case ConditionType.AND:
                        toReturn = FilterTuples(toReturn, matchingTuples);
                        break;
                }
            }

            return toReturn;
        }

        public List<SURLYTuple> Project(List<SURLYAttributeDefinition> attributeDefinitions)
        {
            var toReturn = new List<SURLYTuple>();
            foreach (var surlyTuple in _tuples)
            {
                var attributeList = surlyTuple.Project(attributeDefinitions);
                toReturn.Add(new SURLYTuple().CreateFromAttributeList(attributeList));
            }

            return toReturn;
        }

        public void DeleteAllTuples()
        {
            this._tuples.Clear();
        }


        public void DeleteTuples(List<SURLYTuple> tuples)
        {
            foreach (var surlyTuple in tuples)
            {
                _tuples.Remove(surlyTuple);
            }
        }

        public string Print()
        {
            var toReturn = $"--------------------------------------------\n+{RelationName} \n";

            foreach (var surlyTuple in _tuples)
            {
                toReturn = string.Concat(toReturn, surlyTuple.Print());
            }

            return toReturn;
        }

        private List<SURLYTuple> FilterTuples(List<SURLYTuple> removeFrom, List<SURLYTuple> matchingTuples)
        {
            var toReturn = new List<SURLYTuple>();
            foreach (var surlyTuple in removeFrom)
            {
                var isMatched = false;
                foreach (var tuple in matchingTuples)
                {
                    if (tuple.IsEqual(surlyTuple))
                    {
                        isMatched = true;
                    }
                }

                if (isMatched)
                {
                    toReturn.Add(surlyTuple);
                }
            }

            return toReturn;
        }

        private List<SURLYTuple> AddTuplesDistinct(List<SURLYTuple> addTo, List<SURLYTuple> addFrom)
        {
            var toReturn = new List<SURLYTuple>();
            foreach (var from in addFrom)
            {
                var isUnique = true;
                foreach (var to in addTo)
                {
                    if (to.IsEqual(from))
                    {
                        isUnique = false;
                    }
                }

                if (isUnique)
                {
                    toReturn.Add(from);
                }
            }

            return toReturn;
        }

        private List<SURLYTuple> GetMatchingTuples(SURLYCondition conditional, LinkedList<SURLYTuple> comparedTuples)
        {
            var toReturn = new List<SURLYTuple>();
            foreach (var surlyTuple in comparedTuples)
            {
                if (surlyTuple.IsSelectable(conditional))
                    toReturn.Add(surlyTuple);
            }

            return toReturn;
        }
    }
}
