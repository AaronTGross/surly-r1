﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SURLY.Models
{
    public class SURLYRelationSerializable
    {
        public string RelationName;
        public List<SURLYAttributeDefinition> _definitions = new List<SURLYAttributeDefinition>();
        public List<SURLYTuple> _tuples = new List<SURLYTuple>();


        public SURLYRelation ToDomainObject()
        {
            return new SURLYRelation
            {
                _definitions = _definitions,
                RelationName = RelationName,
                _tuples = ToLinkedList(_tuples)
            };
        }

        private LinkedList<SURLYTuple> ToLinkedList(List<SURLYTuple> listyList)
        {
            var toReturn = new LinkedList<SURLYTuple>();
            listyList.ForEach(x => toReturn.AddLast(x));
            return toReturn;
        } 
    }
}
