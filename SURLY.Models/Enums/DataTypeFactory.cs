﻿namespace SURLY.Models.Enums
{
    public static class DataTypeFactory
    {
        public static DataType Get(string dataTypeName)
        {
            switch (dataTypeName)
            {
                case "CHAR":
                    return DataType.Char;
                default:
                    return DataType.Num;
            }

        }
    }
}
