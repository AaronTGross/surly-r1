namespace SURLY.Models.Enums
{
    public enum ServiceType
    {
        RelationDataService,
        DestroyDataService,
        InsertDataService,
        DeleteDataService,
        PrintDataService,
        SelectDataService,
        ProjectDataService,
        ImportDataService,
        ExportDataService,
        JoinDataService,
        CatalogDataService,
        TempDataService,
        none
    }
}
