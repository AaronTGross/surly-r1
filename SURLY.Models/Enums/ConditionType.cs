﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SURLY.Models.Enums
{
    public enum ConditionType
    {
        AND,
        OR,
        JOIN,
        none
    }
}
