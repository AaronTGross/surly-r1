﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SURLY.Models.Enums
{
    public static class ConditionTypeFactory
    {
        public static ConditionType Get(string condition)
        {
            switch (condition.ToLower())
            {
                case "and":
                    return ConditionType.AND;
                case "or":
                    return ConditionType.OR;
                default:
                    return ConditionType.none;
            }
        }
    }
}
