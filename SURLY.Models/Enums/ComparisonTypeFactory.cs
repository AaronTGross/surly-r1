﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SURLY.Models.Enums
{
    public static class ComparisonTypeFactory
    {
        public static ComparisonType Get(string condition)
        {
            switch (condition.ToLower())
            {
                case "=":
                    return ComparisonType.Equals;
                case ">":
                    return ComparisonType.GreaterThan;
                case ">=":
                    return ComparisonType.GreaterThanOrEqual;
                case "<":
                    return ComparisonType.LessThan;
                case "<=":
                    return ComparisonType.LessThanOrEqual;
                default:
                    return ComparisonType.none;
            }
        }
    }
}
