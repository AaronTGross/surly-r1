﻿
using System.Collections.Generic;
using System.Linq;
using SURLY.Models.Util;

namespace SURLY.Models
{
    public class SURLYTuple
    {
        public List<SURLYAttribute> _attributes = new List<SURLYAttribute>();

        public SURLYTuple CreateFromAttributeList(List<SURLYAttribute> attributes)
        {
            _attributes.AddRange(attributes);

            return this;
        }

        public bool IsSelectable(SURLYCondition conditional)
        {
            foreach (var surlyAttribute in _attributes)
            {
                if (surlyAttribute.IsSelectable(conditional))
                    return true;
            }
            return false;
        }

        public SURLYAttribute FindAttributeByName(string attName)
        {
            return _attributes.FirstOrDefault(x => x.Type.Name == attName);
        }

        public List<SURLYAttribute> JoinAttributes(SURLYTuple tuples, string attributeToSkip)
        {
            var attCopy = new List<SURLYAttribute>(_attributes);
            tuples._attributes.ForEach(x =>
            {
                if (x.Type.Name != attributeToSkip)
                {
                    attCopy.Add(x);
                }
            });

            return attCopy;
        }

        public string Print()
        {
            var toReturn = "";

            foreach (var surlyAttribute in _attributes)
            {
                toReturn = $" {toReturn} {surlyAttribute.Value}";
            }

            return $"{toReturn}\n";
        }

        public List<SURLYAttribute> Project(List<SURLYAttributeDefinition> attributeDefinitions)
        {
            var toReturn = new List<SURLYAttribute>();
            foreach (var surlyAttribute in _attributes)
            {
                foreach (var definition in attributeDefinitions)
                {
                    if (surlyAttribute.IsOfType(definition))
                    {
                        toReturn.Add(surlyAttribute);
                    }
                }
            }

            return toReturn;
        }

        public bool IsEqual(SURLYTuple against)
        {
            var equal = false;

            foreach (var surlyAttribute in _attributes)
            {
                foreach (var againstAttribute in against._attributes)
                {
                    if (surlyAttribute.IsEqual(againstAttribute))
                    {
                        equal = true;
                        break;
                    }

                    equal = false;

                }
                if (!equal)
                {
                    break;
                }
            }

            return equal;
        }
    }
}
