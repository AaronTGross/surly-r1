﻿using System;
using System.Collections.Generic;
using SURLY.Models;
using SURLY.QueryAnalyser.CommandQueryParsers;

namespace SURLY.Service.DataService
{
    public class ProjectDataService : BaseDataService, IRelationallyClosedDataService
    {
        private readonly ProjectQueryParser _queryParser = new ProjectQueryParser();
        List<SURLYAttributeDefinition> _attributeDefinitions = new List<SURLYAttributeDefinition>();

        public override void ConsumeQuery(string query)
        {
            base.ConsumeQuery(query);
            _attributeDefinitions = _queryParser.GetAttributesDefinitions(Query);
        }

        public SURLYRelation ExecuteQuery()
        {
            var projectTuples = Relation.Project(_attributeDefinitions);
            var relation = new SURLYRelation();
            relation.AddTuples(projectTuples);
            return relation;
        }
    }
}
