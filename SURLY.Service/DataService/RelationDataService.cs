﻿using System.Collections.Generic;
using SURLY.Data;
using SURLY.Models;
using SURLY.QueryAnalyser;
using SURLY.QueryAnalyser.CommandQueryParsers;

namespace SURLY.Service.DataService
{
    public class RelationDataService : BaseDataService, IDataService
    {
        private List<SURLYAttributeDefinition> _attributesDefinitions = new List<SURLYAttributeDefinition>();
        private readonly RelationQueryParser _queryParser = new RelationQueryParser();
        private string _relationName = "";

        public override void ConsumeQuery(string query)
        {
            base.Query = query;
            _relationName = new QueryBasics().GetRelationName(Query);
            _attributesDefinitions =_queryParser.GetAttributeDefinitions(Query);
        }

        public void ExecuteQuery()
        {
            DataBase.AddRelation(_relationName, _attributesDefinitions);
            DataBase.AddRelationToCatalog(_relationName);
        }
    }
}
