﻿using System;
using SURLY.Data;

namespace SURLY.Service.DataService
{
    public class PrintDataService : BaseDataService, IDataService
    {
        public override void ConsumeQuery(string query)
        {
            if (query.Length > 5)
            {
                base.ConsumeQuery(query);
            }
        }

        public void ExecuteQuery()
        {
            if (string.IsNullOrEmpty(Query))
            {
                DataBase.PrintAllRelations();
            }
            else
            {
                Console.WriteLine(Relation.Print());
            }
        }
    }
}
