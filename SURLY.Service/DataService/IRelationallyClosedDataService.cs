﻿using SURLY.Models;

namespace SURLY.Service.DataService
{
    public interface IRelationallyClosedDataService
    {
        void ConsumeQuery(string query);
        SURLYRelation ExecuteQuery();
    }
}
