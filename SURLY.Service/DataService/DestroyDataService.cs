﻿using SURLY.Data;

namespace SURLY.Service.DataService
{
    public class DestroyDataService : BaseDataService, IDataService
    {
        public void ExecuteQuery()
        {
            DataBase.DestroyRelation(Relation);
        }
    }
}
