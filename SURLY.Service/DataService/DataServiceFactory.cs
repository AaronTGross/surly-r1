﻿using SURLY.Models.Enums;

namespace SURLY.Service.DataService
{
    public static class DataServiceFactory
    {
        public static IDataService GetService(string query)
        {
            var type = QueryService.QueryService.GetServiceType(query);

            switch (type)
            {
                case ServiceType.RelationDataService:
                    return new RelationDataService();
                case ServiceType.DestroyDataService:
                    return new DestroyDataService();
                case ServiceType.InsertDataService:
                    return new InsertDataService();
                case ServiceType.DeleteDataService:
                    return new DeleteDataService();
                case ServiceType.PrintDataService:
                    return new PrintDataService();
                case ServiceType.ExportDataService:
                    return new ExportDataService();
                case ServiceType.ImportDataService:
                    return new ImportDataService();
                case ServiceType.CatalogDataService:
                    return new CatalogDataService();
                case ServiceType.TempDataService:
                    return new TempDataService();
                default:
                    return null;
            }
        }

        public static IRelationallyClosedDataService GetRelationallyClosedDataServiceService(string query)
        {
            {
                var type = QueryService.QueryService.GetServiceType(query);

                switch (type)
                {
                    case ServiceType.SelectDataService:
                        return new SelectDataService();
                    case ServiceType.ProjectDataService:
                        return new ProjectDataService();
                    case ServiceType.JoinDataService:
                        return new JoinDataService();
                    default:
                        return null;
                }
            }
        }
    }
}
