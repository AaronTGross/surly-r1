﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using SURLY.Data;
using SURLY.Models;
using SURLY.QueryAnalyser.CommandQueryParsers;

namespace SURLY.Service.DataService
{
    public class ExportDataService : IDataService
    {
        private readonly List<SURLYRelation> _relations = new List<SURLYRelation>();
        private readonly ImportExportQueryAnalyser _queryAnalyser = new ImportExportQueryAnalyser();

        public void ConsumeQuery(string query)
        {
            var relationsToXMLify = _queryAnalyser.GetRelationNames(query);
            foreach (var relationName in relationsToXMLify)
            {
                _relations.Add(DataBase.GetRelationByName(relationName));
            }

        }

        public void ExecuteQuery()
        {
            _relations.ForEach(x =>
            {
                foreach (var surlyRelation in _relations)
                {
                    using (var stream = new FileStream($"C:\\Users\\AaronGross\\Desktop\\Surly-r1\\{surlyRelation.RelationName}.xml", FileMode.Create))
                    {
                        var serializer = new XmlSerializer(typeof(SURLYRelationSerializable));
                        var serializableRelation = x.ToSerializable();
                        serializer.Serialize(stream, serializableRelation);
                    }
                }
            });
        }
    }
}
