﻿using System;
using SURLY.Data;

namespace SURLY.Service.DataService
{
    public class CatalogDataService : IDataService
    {
        public void ConsumeQuery(string query)
        {
        }

        public void ExecuteQuery()
        {
            var relation = DataBase.GetRelationByName("Catalog");
            Console.WriteLine(relation.Print());
        }
    }
}
