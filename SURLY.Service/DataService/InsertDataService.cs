﻿using System.Collections.Generic;
using SURLY.Models;
using SURLY.QueryAnalyser.CommandQueryParsers;

namespace SURLY.Service.DataService
{
    public class InsertDataService : BaseDataService, IDataService
    {
        private List<SURLYAttribute> _attributes = new List<SURLYAttribute>();
        private readonly InsertQueryParser _queryParser = new InsertQueryParser();

        public override void ConsumeQuery(string query)
        {
            base.ConsumeQuery(query);
            _attributes = _queryParser.GetAttributes(Query);
            _attributes.ForEach(x =>
            {
                if (x.Value.Contains("\'"))
                {
                    x.Value = x.Value + "\'";
                }
            });

            var index = 0;
            foreach (var surlyAttributeDefinition in Relation.GetTypesDefinitions())
            {
                _attributes[index].SetType(surlyAttributeDefinition);
                index++;
            }
        }

        public void ExecuteQuery()
        {
            var tuple = new SURLYTuple().CreateFromAttributeList(_attributes);
            Relation.AddTuple(tuple);
        }
    }
}
