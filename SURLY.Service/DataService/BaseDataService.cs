﻿using SURLY.Data;
using SURLY.Models;
using SURLY.QueryAnalyser;

namespace SURLY.Service.DataService
{
    public abstract class BaseDataService 
    {
        protected string Query;
        protected SURLYRelation Relation;

        public virtual void ConsumeQuery(string query)
        {
            Query = query;
            SetRelation();
        }

        private void SetRelation()
        {
            var relationName = new QueryBasics().GetRelationName(Query);
            Relation = DataBase.GetRelationByName(relationName);
        }
    }
}
