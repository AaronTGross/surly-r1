﻿using System;
using System.Collections.Generic;
using System.Linq;
using SURLY.Data;
using SURLY.Models;
using SURLY.Models.Enums;
using SURLY.Models.Util;
using SURLY.QueryAnalyser.CommandQueryParsers;

namespace SURLY.Service.DataService
{
    public class JoinDataService : IRelationallyClosedDataService
    {
        private readonly List<SURLYRelation> _relations = new List<SURLYRelation>();
        private readonly List<SURLYCondition> _joinConditions = new List<SURLYCondition>();
        private readonly JoinQueryParser _queryParser = new JoinQueryParser();

        public void ConsumeQuery(string query)
        {
            var relationNames = _queryParser.GetRelations(query);
            relationNames.ForEach(x => _relations.Add(DataBase.GetRelationByName(x)));

            _joinConditions.AddRange(_queryParser.GetJoinConditions(query));
        }

        public SURLYRelation ExecuteQuery()
        {
            var relation = new SURLYRelation();
            _joinConditions.ForEach(x =>
            {
                if (x.ConditionType == ConditionType.JOIN)
                {
                    HandleComparisons(x, relation);
                }
            });

            if (WhereConditionInConditions())
            {
                var relationToReturn = new SURLYRelation();
                relationToReturn.AddTuples(relation.Select(_joinConditions));
                return relationToReturn;
            }
            return relation;
        }

        private bool WhereConditionInConditions()
        {
            var returnValue = false;
            _joinConditions.ForEach(x =>
            {
                if (x.ConditionType == ConditionType.AND || x.ConditionType == ConditionType.OR)
                {
                    returnValue = true;
                }
            });
            return returnValue;
        }


        private void HandleComparisons(SURLYCondition x, SURLYRelation addTo)
        {
            var relation1 = DataBase.GetRelationByName(x.AttributeToCompare.Split('.')[0].ToUpper());
            var relation2 = DataBase.GetRelationByName(x.AttributeToCompareAgainst.Split('.')[0].ToUpper());

            switch (x.ComparisonType)
            {
                case ComparisonType.Equals:
                    HandleEquals(x, relation1, relation2, addTo);
                    break;
                case ComparisonType.GreaterThan:
                    HandleGreaterThan(x, relation1, relation2, addTo);
                    break;
                case ComparisonType.GreaterThanOrEqual:
                    HandleGreaterThanOrEqual(x, relation1, relation2, addTo);
                    break;
                case ComparisonType.LessThan:
                    HandleLessThan(x, relation1, relation2, addTo);
                    break;
                case ComparisonType.LessThanOrEqual:
                    HandleLessThanOrEqual(x, relation1, relation2, addTo);
                    break;
            }
        }

        private void HandleEquals(SURLYCondition x, SURLYRelation relation1, SURLYRelation relation2, SURLYRelation addTo)
        {
            relation1._tuples.ToList().ForEach(tuple1 =>
            {
                var att1 = tuple1.FindAttributeByName(x.AttributeToCompare.Split('.')[1].ToUpper());

                relation2._tuples.ToList().ForEach(tuple2 =>
                {
                    var att2 = tuple2.FindAttributeByName(x.AttributeToCompareAgainst.Split('.')[1].ToUpper());
                    if (att1 != null && att2 != null && att1.Value == att2.Value)
                    {
                        var newTuple = new SURLYTuple
                        {
                            _attributes = tuple1.JoinAttributes(tuple2, att2.Type.Name)
                        };
                        addTo.AddTuple(newTuple);
                    }
                });
            });
        }

        private void HandleGreaterThan(SURLYCondition x, SURLYRelation relation1, SURLYRelation relation2,
            SURLYRelation addTo)
        {
            relation1._tuples.ToList().ForEach(tuple1 =>
            {
                var att1 = tuple1.FindAttributeByName(x.AttributeToCompare.Split('.')[1].ToUpper());

                relation2._tuples.ToList().ForEach(tuple2 =>
                {
                    var att2 = tuple2.FindAttributeByName(x.AttributeToCompareAgainst.Split('.')[1].ToUpper());
                    if ((int)att1.Type.Type == 0 ? att1.Value.Length > att2.Value.Length : int.Parse(att1.Value) > int.Parse(att2.Value))
                    {
                        var newTuple = new SURLYTuple
                        {
                            _attributes = tuple1.JoinAttributes(tuple2, att2.Type.Name)
                        };
                        addTo.AddTuple(newTuple);
                    }
                });
            });
        }

        private void HandleGreaterThanOrEqual(SURLYCondition x, SURLYRelation relation1, SURLYRelation relation2,
            SURLYRelation addTo)
        {
            relation1._tuples.ToList().ForEach(tuple1 =>
            {
                var att1 = tuple1.FindAttributeByName(x.AttributeToCompare.Split('.')[1].ToUpper());

                relation2._tuples.ToList().ForEach(tuple2 =>
                {
                    var att2 = tuple2.FindAttributeByName(x.AttributeToCompareAgainst.Split('.')[1].ToUpper());
                    if ((int)att1.Type.Type == 0 ? att1.Value.Length <= att2.Value.Length : int.Parse(att1.Value) <= int.Parse(att2.Value))
                    {
                        var newTuple = new SURLYTuple
                        {
                            _attributes = tuple1.JoinAttributes(tuple2, att2.Type.Name)
                        };
                        addTo.AddTuple(newTuple);
                    }
                });
            });
        }

        private void HandleLessThan(SURLYCondition x, SURLYRelation relation1, SURLYRelation relation2,
            SURLYRelation addTo)
        {
            relation1._tuples.ToList().ForEach(tuple1 =>
            {
                var att1 = tuple1.FindAttributeByName(x.AttributeToCompare.Split('.')[1].ToUpper());

                relation2._tuples.ToList().ForEach(tuple2 =>
                {
                    var att2 = tuple2.FindAttributeByName(x.AttributeToCompareAgainst.Split('.')[1].ToUpper());
                    if ((int)att1.Type.Type == 0 ? att1.Value.Length > att2.Value.Length : int.Parse(att1.Value) > int.Parse(att2.Value))
                    {
                        var newTuple = new SURLYTuple
                        {
                            _attributes = tuple1.JoinAttributes(tuple2, att2.Type.Name)
                        };
                        addTo.AddTuple(newTuple);
                    }
                });
            });
        }

        private void HandleLessThanOrEqual(SURLYCondition x, SURLYRelation relation1, SURLYRelation relation2,
            SURLYRelation addTo)
        {
            relation1._tuples.ToList().ForEach(tuple1 =>
            {
                var att1 = tuple1.FindAttributeByName(x.AttributeToCompare.Split('.')[1].ToUpper());

                relation2._tuples.ToList().ForEach(tuple2 =>
                {
                    var att2 = tuple2.FindAttributeByName(x.AttributeToCompareAgainst.Split('.')[1].ToUpper());
                    if ((int)att1.Type.Type == 0 ? att1.Value.Length >= att2.Value.Length : int.Parse(att1.Value) >= int.Parse(att2.Value))
                    {
                        var newTuple = new SURLYTuple
                        {
                            _attributes = tuple1.JoinAttributes(tuple2, att2.Type.Name)
                        };
                        addTo.AddTuple(newTuple);
                    }
                });
            });
        }
    }
}
