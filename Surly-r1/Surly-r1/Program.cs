﻿using System;
using System.IO;
using SURLY.Models;
using SURLY.Service.DataService;
using SURLY.Service.QueryService;

namespace Surly_r1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter:\n\t-file to run pre-made queries\n\t-a surly command");
            while (true)
            {

                var query = Console.ReadLine().ToUpper();
                // check user input
                if (query.Equals("file", StringComparison.OrdinalIgnoreCase))
                {
                    var filePath = "C:\\Users\\AaronGross\\Desktop\\Surly-r1\\relation.txt";

                    try
                    {
                        // read the input file
                        var fileText = File.ReadAllText(filePath);
                        //break the file up into individual queries
                        var queries = QueryService.GetQueries(fileText);

                        //implement the queries
                        foreach (var q in queries)
                        {
                            RunQuery(q);
                        }

                    }
                    catch
                    {
                        Console.WriteLine("Cannot find file at: " + filePath);
                        Console.WriteLine("Check that file path is correct and retry.");
                    }
                }
                else
                {
                    RunQuery(query);
                }

            }
        }

        private static void RunQuery(string query)
        {
            try
            {
                var service = DataServiceFactory.GetService(query);
                //allows whatever meta data and set up needs to happen for the query to be ran
                service.ConsumeQuery(query.ToUpper());

                //does the do
                service.ExecuteQuery();
            }
            catch (Exception)
            {

                Console.WriteLine("There was an issue trying to run: " + query);
            }

        }
    }
}
