﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using SURLY.Models;
using SURLY.Models.Enums;

namespace SURLY.Data
{
    public static class DataBase
    {
        private static readonly SURLYAttributeDefinition _catalogType = new SURLYAttributeDefinition
        {
            Length = 10,
            Name = "Name",
            Type = DataType.Char
        }; 
        private static readonly List<SURLYRelation> Relations = new List<SURLYRelation>
        {
            new SURLYRelation
            {
                RelationName = "Catalog",
                _definitions = new List<SURLYAttributeDefinition>
                {
                    _catalogType
                },
                _tuples = new LinkedList<SURLYTuple>()
            }
        };

        public static void AddRelation(string relationName, List<SURLYAttributeDefinition> attributes)
        {
            var relation = new SURLYRelation(relationName);
            foreach (var surlyAttribute in attributes)
            {
                relation.AddAttributeDefinitions(surlyAttribute);
            }

            Relations.Add(relation);
        }

        public static void AddRelation(SURLYRelation relation)
        {
            //check to see if the relation exists
            if (RelationExists(relation))
            {
                var toUpdate = GetRelationByName(relation.RelationName);
                toUpdate.AddTuples(relation._tuples);
            }
            else
            {
                Relations.Add(relation);
            }

        }

        public static void AddRelationToCatalog(string relationName)
        {
            var catalog = GetRelationByName("Catalog");
            catalog.AddTuple(new SURLYTuple
            {
                _attributes = new List<SURLYAttribute>
                {
                    new SURLYAttribute
                    {
                        Type = _catalogType,
                        Value = relationName
                    }
                }
            });
        }

        public static SURLYRelation GetRelationByName(string name)
        {
            return Relations.FirstOrDefault(x => x.RelationName == name.Trim());
        }

        public static void RemoveRelationFromCatalog(string relationName)
        {
            var catalog = GetRelationByName("Catalog");
            var toRemove = new SURLYTuple();
            foreach (var surlyTuple in catalog._tuples)
            {
                var attribute = surlyTuple.FindAttributeByName("Name");
                if (attribute.Value == relationName)
                {
                    toRemove = surlyTuple;
                    break;
                }
            }
            catalog._tuples.Remove(toRemove);
        }

        public static void DestroyRelation(SURLYRelation relation)
        {
            Relations.Remove(relation);
        }

        public static void PrintAllRelations()
        {
            var toPrint = "-------------------------------------------\n";

            foreach (var surlyRelation in Relations)
            {
                toPrint = $"{toPrint} {surlyRelation.Print()}\n\n";
            }

            Console.WriteLine(toPrint);
        }

        private static bool RelationExists(SURLYRelation relation)
        {
            foreach (var surlyRelation in Relations)
            {
                if (relation.RelationName == surlyRelation.RelationName)
                {
                    return true;
                }
            }

            return false;
        }

    }
}
