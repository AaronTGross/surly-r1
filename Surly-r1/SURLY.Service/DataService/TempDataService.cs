﻿using System.Collections.Generic;
using System.Linq;
using SURLY.Data;
using SURLY.Models;

namespace SURLY.Service.DataService
{
    public class TempDataService : IDataService
    {
        private string _relationNameToCreate = "";
        private string _queryToRun = "";
        public void ConsumeQuery(string query)
        {
            _relationNameToCreate = query.Split(' ')[1];
            _queryToRun = RemoveToEqualSign(query);
        }

        public void ExecuteQuery()
        {
            DataBase.AddRelation(_relationNameToCreate, new List<SURLYAttributeDefinition>());
            var relation = DataBase.GetRelationByName(_relationNameToCreate);
            relation.AddTuples(RunQuery(_queryToRun)._tuples);
            DataBase.GetRelationByName(_relationNameToCreate).Print();
        }

        private string RemoveToEqualSign(string query)
        {
            var removeToIndex = 0;

            var queryArray = query.Split(' ').ToList();
            foreach (var sub in queryArray)
            {
                if (sub == "=")
                {
                    break;
                }
                else
                {
                    removeToIndex += sub.Length + 1;
                }
            }

            return query.Remove(0, removeToIndex + 2);
        }

        private SURLYRelation RunQuery(string query)
        {
            var relationService = DataServiceFactory.GetRelationallyClosedDataServiceService(query);
            //allows whatever meta data and set up needs to happen for the query to be ran
            relationService.ConsumeQuery(query);

            //does the do
            return relationService.ExecuteQuery();
        }
    }
}
