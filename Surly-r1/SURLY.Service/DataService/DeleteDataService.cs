﻿using System.Collections.Generic;
using SURLY.Data;
using SURLY.Models.Util;
using SURLY.QueryAnalyser.CommandQueryParsers;

namespace SURLY.Service.DataService
{
    public class DeleteDataService : BaseDataService, IDataService
    {
        private readonly ConditionedQueryParser _queryParser = new ConditionedQueryParser();
        private List<SURLYCondition> _whereConditions = new List<SURLYCondition>();
         
        public override void ConsumeQuery(string query)
        {
            base.ConsumeQuery(query);
            if (QueryContainsWhere())
            {
                _whereConditions.AddRange(_queryParser.GetConditions(Query));
            }

        }

        public void ExecuteQuery()
        {
            if (!QueryContainsWhere())
            {
                Relation.DeleteAllTuples();
            }
            else
            {
                var selectedTuples = Relation.Select(_whereConditions);
                Relation.DeleteTuples(selectedTuples);
            }
        }

        private bool QueryContainsWhere()
        {
            return Query.ToLower().Contains("where");
        }
    }
}
