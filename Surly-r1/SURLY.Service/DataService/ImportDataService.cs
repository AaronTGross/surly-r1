﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using SURLY.Data;
using SURLY.Models;
using SURLY.QueryAnalyser.CommandQueryParsers;

namespace SURLY.Service.DataService
{
    public class ImportDataService : IDataService
    {
        private readonly ImportExportQueryAnalyser _queryAnalyser = new ImportExportQueryAnalyser();
        private readonly Dictionary<string, string> _xmlFileNames = new Dictionary<string, string>();

        public void ConsumeQuery(string query)
        {
            var fileName = _queryAnalyser.GetFileNames(query);
            foreach (var keyValuePair in fileName)
            {
                _xmlFileNames.Add(keyValuePair.Key, keyValuePair.Value);
            }
        }

        public void ExecuteQuery()
        {
            foreach (var xmlFileName in _xmlFileNames)
            {
                using (var stream = new FileStream(xmlFileName.Value, FileMode.Open))
                {
                    var deserialize = new XmlSerializer(typeof(SURLYRelationSerializable));
                    var relation = (SURLYRelationSerializable) deserialize.Deserialize(stream);
                    relation.RelationName = xmlFileName.Key;
                    var domainModel = relation.ToDomainObject();
                    DataBase.AddRelation(domainModel);
                }
            }
        }
    }
}
