﻿using System;
using System.Collections.Generic;
using SURLY.Models;
using SURLY.Models.Util;
using SURLY.QueryAnalyser.CommandQueryParsers;

namespace SURLY.Service.DataService
{
    public class SelectDataService : BaseDataService, IRelationallyClosedDataService
    {
        private readonly ConditionedQueryParser _queryParser = new ConditionedQueryParser();
        private readonly List<SURLYCondition> _whereConditions = new List<SURLYCondition>();

        public override void ConsumeQuery(string query)
        {
            base.ConsumeQuery(query);
            if (QueryContainsWhere())
            {
                _whereConditions.AddRange(_queryParser.GetConditions(Query));
            }

        }


        public SURLYRelation ExecuteQuery()
        {
            var selectedTuples = Relation.Select(_whereConditions);
            var relation = new SURLYRelation();
            relation.AddTuples(selectedTuples);
            return relation;
        }

        private bool QueryContainsWhere()
        {
            return Query.ToLower().Contains("where");
        }
    }
}
