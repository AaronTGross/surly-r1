﻿namespace SURLY.Service.DataService
{
    public interface IDataService
    {
        void ConsumeQuery(string query);
        void ExecuteQuery();
    }
}
