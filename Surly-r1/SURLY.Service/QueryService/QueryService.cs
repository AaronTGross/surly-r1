﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SURLY.Models.Enums;
using SURLY.QueryAnalyser;
using SURLY.Service.DataService;

namespace SURLY.Service.QueryService
{
    public static class QueryService
    {
        public static ServiceType GetServiceType(string query)
        {
            var command = GetQueryBasics().GetCommand(query);
            switch (command)
            {
                case "relation":
                    return ServiceType.RelationDataService;
                case "destroy":
                    return ServiceType.DestroyDataService;
                case "insert":
                    return ServiceType.InsertDataService;
                case "delete":
                    return ServiceType.DeleteDataService;
                case "print":
                    return  ServiceType.PrintDataService;
                case "select":
                    return ServiceType.SelectDataService;
                case "project":
                    return ServiceType.ProjectDataService;
                case "import":
                    return ServiceType.ImportDataService;
                case "export":
                    return ServiceType.ExportDataService;
                case "join":
                    return ServiceType.JoinDataService;
                case "catalog":
                    return ServiceType.CatalogDataService;
                case "temp":
                    return ServiceType.TempDataService;
                default:
                    return ServiceType.none;
            }
        }

        public static List<string> GetQueries(string textFile)
        {
            return GetQueryBasics().ConvertStringOfMultipleQueriesIntoListsOfQueries(textFile);
        }

        private static QueryBasics GetQueryBasics()
        {
            return new QueryBasics();
        }
    }
}
