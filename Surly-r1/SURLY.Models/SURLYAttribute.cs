﻿using SURLY.Models.Enums;
using SURLY.Models.Util;

namespace SURLY.Models
{
    public class SURLYAttribute
    {
        public string Value { get; set; }
        public SURLYAttributeDefinition Type { get; set; }

        public void SetValue(string value)
        {
            Value = value;
        }

        public void SetType(SURLYAttributeDefinition type)
        {
            Type = type;
        }

        public bool IsSelectable(SURLYCondition condition)
        {
            var selectable = false;

            if (condition.ConditionType == ConditionType.JOIN)
            {
                return selectable;
            }

            if (Type.Name == condition.AttributeToCompare.ToUpper())
            {
                selectable = Comparisons(condition);
                if (selectable)
                {
                    return true;
                }

            }
            else
            {
                if (condition.AttributeToCompare.Contains("."))
                {
                    if (Type.Name == condition.AttributeToCompare.Split('.')[1].ToUpper())
                    {
                        var valComparedAgainst = condition.ValueComparedAgainst;
                        if (valComparedAgainst.Contains("\'"))
                        {
                            valComparedAgainst = valComparedAgainst + "\'";
                        }
                        var newCondtion = new SURLYCondition
                        {
                            AttributeToCompare = condition.AttributeToCompare.Split('.')[1].ToUpper(),
                            ConditionType = condition.ConditionType,
                            AttributeToCompareAgainst = condition.AttributeToCompareAgainst,
                            ComparisonType = condition.ComparisonType,
                            ValueComparedAgainst = valComparedAgainst
                        };
                        selectable = Comparisons(newCondtion);
                        if (selectable)
                        {
                            return true;
                        }

                    }
                }
            }
            return selectable;
        }

        public bool IsEqual(SURLYAttribute against)
        {
            return against.Value == this.Value && against.Type == this.Type;
        }

        public bool IsOfType(SURLYAttributeDefinition definition)
        {
            return Type.Name == definition.Name;
        }

        private bool Comparisons(SURLYCondition condition)
        {
            var conditionValue = condition.ValueComparedAgainst.ToUpper();

            switch (condition.ComparisonType)
            {
                case ComparisonType.Equals:
                    return EqualsCheck(conditionValue);
                case ComparisonType.LessThan:
                    return LessThanCheck(conditionValue);
                case ComparisonType.LessThanOrEqual:
                    return LessThanOrEqualCheck(conditionValue);
                case ComparisonType.GreaterThan:
                    return GreaterThanCheck(conditionValue);
                case ComparisonType.GreaterThanOrEqual:
                    return GreaterThanOrEqualCheck(conditionValue);
                default:
                    return false;
            }
        }

        private bool EqualsCheck(string conditionalValue)
        {
            if (Type.Type == DataType.Char)
            {
                return Value == conditionalValue;
            }

            return Value == conditionalValue;
        }

        private bool LessThanCheck(string conditionalValue)
        {
            if (Type.Type == DataType.Char)
            {
                return Value.Length < conditionalValue.Length;
            }

            return int.Parse(Value) < int.Parse(conditionalValue);
        }


        private bool LessThanOrEqualCheck(string conditionalValue)
        {
            if (Type.Type == DataType.Char)
            {
                return Value.Length <= conditionalValue.Length;
            }

            return int.Parse(Value) <= int.Parse(conditionalValue);
        }

        private bool GreaterThanCheck(string conditionalValue)
        {
            if (Type.Type == DataType.Char)
            {
                return Value.Length > conditionalValue.Length;
            }

            return int.Parse(Value) > int.Parse(conditionalValue);
        }

        private bool GreaterThanOrEqualCheck(string conditionalValue)
        {
            if (Type.Type == DataType.Char)
            {
                return Value.Length >= conditionalValue.Length;
            }

            return int.Parse(Value) >= int.Parse(conditionalValue);
        }

    }
}
