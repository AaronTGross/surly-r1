﻿using SURLY.Models.Enums;

namespace SURLY.Models
{
    public class SURLYAttributeDefinition
    {
        public string Name { get; set; }
        public DataType Type { get; set; }
        public int Length { get; set; }

        public SURLYAttributeDefinition FromString(string attributeString)
        {
            var atts = attributeString.Trim().Split(' ');
            Name = atts[0];
            Type = DataTypeFactory.Get(atts[1]);
            Length = int.Parse(atts[2]);

            return this;
        } 
    }
}
