﻿using SURLY.Models.Enums;

namespace SURLY.Models.Util
{
    public class SURLYCondition
    {
        public ConditionType ConditionType { get; set; }
        public ComparisonType ComparisonType { get; set; }

        public string AttributeToCompare { get; set; }
        public string ValueComparedAgainst { get; set; }

        //only needed for joins because they may or may not be compared against different attributes 
        public string AttributeToCompareAgainst { get; set; }
    }
}
