﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SURLY.Models.Util
{
    public class BreakSymbols
    {
        private List<string> _brekaSymbols;

        public List<string> GetSimpleBreaks()
        {
            _brekaSymbols = new List<string>
            {
                " ",
                "\'",
                "(",
                ")"
            };
            return _brekaSymbols;
        }

        public List<string> GetBreaksForGrouping()
        {
            _brekaSymbols = new List<string>
            {
                "(",
                ")"
            };
            return _brekaSymbols;
        }
    }
}
