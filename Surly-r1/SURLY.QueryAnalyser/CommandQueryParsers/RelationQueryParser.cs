﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using SURLY.Models;

namespace SURLY.QueryAnalyser.CommandQueryParsers
{
    public class RelationQueryParser
    {
        public List<SURLYAttributeDefinition> GetAttributeDefinitions(string query)
        {
            var attributeDefString = ToAttributeString(query);
            var attributeDefArray = ToAttributeArray(attributeDefString);

            return FromStringToModel(attributeDefArray);
        }

        private string[] ToAttributeArray(string attributeString)
        {
            return attributeString.Split(',');
        }

        private string ToAttributeString(string query)
        {
            var index = query.IndexOf('(');
            var semiclean = query.Remove(0, index + 1);
            var clean = semiclean.Remove(semiclean.Length - 1);

            return clean;
        }

        private List<SURLYAttributeDefinition> FromStringToModel(string[] attributes)
        {
            var attributesObjects = new List<SURLYAttributeDefinition>();
            foreach (var attribute in attributes)
            {
                attributesObjects.Add(new SURLYAttributeDefinition().FromString(attribute));
            }

            return attributesObjects;
        }
    }
}
