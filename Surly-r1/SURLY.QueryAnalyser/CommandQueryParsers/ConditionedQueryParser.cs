﻿using System;
using System.Collections.Generic;
using SURLY.Models.Util;

namespace SURLY.QueryAnalyser.CommandQueryParsers
{
    public class ConditionedQueryParser
    {
        private readonly CustomQueryParser _customQueryParser = new CustomQueryParser();

        public List<SURLYCondition> GetConditions(string query)
        {
            var cleanedConditionString = GetCleanedCondition(query);
            var temp = _customQueryParser.GetConditionsConderingMultipleConditions(cleanedConditionString);
            var conditionObjs = new QueryBasics().GetConditions(temp);

            return conditionObjs;
        }

        private string GetCleanedCondition(string query)
        {
            var splitQuery = query.Split(' ');
            //adding two because there are going to be spaces between the command and the relation name
            var index = splitQuery[0].Length + splitQuery[1].Length + 2;
            //specifically to delete the where key word
            index += 6;
            return query.Remove(0, index);
        }
    }
}
