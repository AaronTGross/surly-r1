﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SURLY.QueryAnalyser.CommandQueryParsers
{
    public class ImportExportQueryAnalyser
    {
        public List<string> GetRelationNames(string query)
        {
            return removeFirstWord(query);
        }

        public List<KeyValuePair<string, string>> GetFileNames(string query)
        {
            var toReturn =  new List<KeyValuePair<string, string>>();
            foreach (var filePath in removeFirstWord(query))
            {
                toReturn.Add(new KeyValuePair<string, string>(ParseRelationNameFromFilePath(filePath), filePath.Trim('\'')));
            }

            return toReturn;
        }

        private List<string> removeFirstWord(string query)
        {
            var commandLength = query.Split(' ')[0].Length + 1;
            query = query.Remove(0, commandLength);
            return query.Split(' ').ToList();
        }

        private string ParseRelationNameFromFilePath(string filePath)
        {
            var toReturn = "";
            var pathArray = filePath.Trim().ToCharArray();
            for (int i = pathArray.Length - 5; i >= 0; i--)
            {
                if (pathArray[i] == '\\')
                {
                    break;
                }

                toReturn = $"{pathArray[i]}{toReturn}";
            }

            return toReturn;
        }
    }
}
