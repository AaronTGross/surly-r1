﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using SURLY.Models.Enums;
using SURLY.Models.Util;

namespace SURLY.QueryAnalyser
{
    public class QueryBasics
    {
        private CustomQueryParser _queryParser = new CustomQueryParser();
        public string GetCommand(string query)
        {
            return query.Split(' ')[0].ToLower();
        }

        public string GetRelationName(string query)
        {
            return query.Split(' ')[1];
        }

        public List<string> ConvertStringOfMultipleQueriesIntoListsOfQueries(string textBlob)
        {
            var cleanedQueries = new List<string>();
            var queryArray = textBlob.Split(';');
            foreach (var queryString in queryArray)
            {
                if (!string.IsNullOrEmpty(queryString))
                    cleanedQueries.Add(queryString.Replace("\r\n", ""));
            }

            return cleanedQueries;
        }

        public List<SURLYCondition> GetConditions(List<string> conditionStrings)
        {
            var toReturn = new List<SURLYCondition>();
            foreach (var conditionString in conditionStrings)
            {
                var condition = new SURLYCondition();
                condition.ConditionType = ParseAndGetConditionType(conditionString);
                condition.ComparisonType = ParseAndGetComparisonType(conditionString);
                condition.AttributeToCompare = ParseAttribute(conditionString);
                condition.ValueComparedAgainst = ParseValue(conditionString);

                toReturn.Add(condition);
            }

            return toReturn;
        }

        private string ParseValue(string condition)
        {
            var temp = _queryParser.GetAsListOfStringsConsideringSpecialCharacters(condition);
            return temp[3].Replace("\'", "");
        }

        private string ParseAttribute(string condition)
        {
            return condition.Split(' ')[1];
        }

        private ConditionType ParseAndGetConditionType(string condition)
        {
            return ConditionTypeFactory.Get(condition.Split(' ')[0]);
        }

        private ComparisonType ParseAndGetComparisonType(string condition)
        {
            var temp = ComparisonTypeFactory.Get(condition.Split(' ')[2]);
            return ComparisonTypeFactory.Get(condition.Split(' ')[2]);
        }
    }
}
