﻿using System;
using System.IO;
using SURLY.Models;
using SURLY.Service.DataService;
using SURLY.Service.QueryService;

namespace Surly_r1
{
    class Program
    {
        static void Main(string[] args)
        {
            // read the input file
            var fileText = File.ReadAllText("C:\\Users\\AaronGross\\Desktop\\Surly-r1\\relation.txt");

            //break the file up into individual queries
            var queries = QueryService.GetQueries(fileText);

            //implement the queries
            foreach (var query in queries)
            {
               DoesTheDo(query);
            }
            while (true)
            {
                Console.WriteLine("\n\n\n Now Enter Commands:");
                var query = Console.ReadLine();

                DoesTheDo(query);
            }
        }

        private static void DoesTheDo(string query)
        {
            //try
            //{
                var service = DataServiceFactory.GetService(query);
                //allows whatever meta data and set up needs to happen for the query to be ran
                service.ConsumeQuery(query.ToUpper());

                //does the do
                service.ExecuteQuery();
            //}
            //catch (Exception)
            //{
                
            //    Console.WriteLine("There was an issue trying to run: " + query);
            //}

        }
    }
}
